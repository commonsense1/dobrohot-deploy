# Приложение Доброхот

## Frontend репозиторий

https://gitlab.com/commonsense1/dobrohot-frontend

## Backend репозиторий

https://gitlab.com/commonsense1/dobrohot-backend

## JSON-RPC спецификация

https://gitlab.com/commonsense1/dobrohot-specification

### Быстрый старт

Для разворачивания приложения в локальном окружении необходимы установленные:

- `docker >= 18.0` _(установить: `curl -fsSL get.docker.com | sudo sh`)_
- `docker-compose >= 1.22` _([инструкция по установке][install_compose])_
- `make >= 4.1` _(установка: `apt-get install make`, 99% что он уже стоит)_

### Последовательность установки

1. Клонировать содержимое данного репозитория

```
    git clone https://gitlab.com/commonsense1/dobrohot-deploy.git
```

2. Перейти в папку с содержимым

```
    cd dobrohot-deploy
```

3. Убедиться, что порты 8080 и 8081 свободны, `docker engine` запущен

4. Из командной строки выполнить скрипт

```
    sh run-me.sh
```

5. Приложение должно быть доступно адресу http://localhost:8081


### Запуск спецификации (опционально)

1. Из командной строки выполнить скрипт

```
    sh run-spec.sh
```

2. Зайти на http://localhost:3000

### Production сервер

http://185.41.162.107:8081/