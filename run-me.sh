#!/bin/sh

git clone https://gitlab.com/commonsense1/dobrohot-frontend.git
cd dobrohot-frontend && cp .env.example .env
make build-image
make init
make up

cd .. && git clone https://gitlab.com/commonsense1/dobrohot-backend.git
cd dobrohot-backend
make install
make init
make up

echo "The Dobrohot application is running on http://localhost:8081"